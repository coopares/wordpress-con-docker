# WordPress con Docker Compose
[Tutorial](https://www.digitalocean.com/community/tutorials/how-to-install-wordpress-with-docker-compose-es)

## Servidor
linode: ubuntu-test-docker-wordpress  
ip: 45.79.84.14

### dns - domain
Registros en Cloudflare
```bash
A > pruebas.arbusta.ar > 45.79.84.14
A > www.pruebas.arbusta.ar > 45.79.84.14
```

### Configuración inicial server
```bash
$ ssh -i ~/ruta/key root@45.79.84.14
# apt-get update
# apt-get upgrade
# apt-get install mc zip gzip tar ccze fail2ban ufw
# adduser plataformas
# usermod -aG sudo plataformas
# rsync --archive --chown=plataformas:plataformas ~/.ssh /home/plataformas/
# nano /etc/ssh/sshd_config
	Port 1234
	PermitRootLogin no
	AllowUsers plataformas
	PasswordAuthentication no
	PermitEmptyPasswords no
# service ssh restart
# ufw allow http
# ufw allow https
# ufw allow 1234
# ufw enable
```

Al menos en Linode tuve que configurar manualmente la key:
```
$ mkdir -p ~/.ssh && touch ~/.ssh/authorized_keys
$ chmod 700 ~/.ssh && chmod 600 ~/.ssh/authorized_keys
$ nano ~/.ssh/authorized_keys
```
Entonces:
```
$ ssh -p 1234 -i ~/ruta/key plataformas@45.79.84.14
```

### Configuración local ssh:
```bash
$ nano ~/.ssh/config
## Docker
Host docker
HostName 45.79.84.14
Port 1234
User plataformas
IdentityFile ~/ruta/key
```

## docker
### install
```bash
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update
apt-cache policy docker-ce
sudo apt install docker-ce
```

### docker-compose
[Check releases](https://github.com/docker/compose/releases)
```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
```

# Tutorial Digital Ocean
## Paso 1: Definir la configuración del servidor web
```bash
$ mkdir docker_wordpress && cd docker_wordpress
$ mkdir nginx-conf
$ nano nginx-conf/nginx.conf
$ nano .env
$ nano .gitignore
$ nano .dockerignore
```

## Paso 2: Definir variables de entorno
[PENDIENTE]




### docker-compose up
La primera vez me tiró este error:
```bash
$ docker-compose up -d
ERROR: Couldn't connect to Docker daemon at http+docker://localhost - is it running?
If it's at a non-standard location, specify the URL with the DOCKER_HOST environment variable.
```
[Fuente](https://techoverflow.net/2019/03/16/how-to-fix-error-couldnt-connect-to-docker-daemon-at-httpdocker-localhost-is-it-running/)  
Revisar permisos:
```bash
sudo usermod -a -G docker $USER
sudo systemctl enable docker # Auto-start on boot
sudo systemctl start docker # Start right now